<?php

/*
1.  Установить и настроить Linux, кто не успел до настоящего времени.
2.  Доделать лабораторные работы, кто не успел до настоящего времени
3.  Создать страницу своего будущего резюме (гипотетического).
Создать переменные для каждого элемента резюме со значениями соответствующего типа. На странице должна быть следующая информация:
     1) ФИО (string)
     2) Фотография (string - ссылка на фото в интернете)
     3) Название желаемой должности (string)
     4) Общая информация о себе (string в HEREDOC или NEWDOC)
     5) Желаемый уровень заработной платы (integer)
     6) Опыт работы (присвоить тип float, например 2.5)
     7) Город проживания (string)
     8) Готов к переезду (boolean)
     9) Электронная почта (string)
     10) Номер телефона (string)

Вывести на экран переменные в соответствующих блоках страницы

PS. Для вывода boolean значения можно использовать такую конструкцию: 
echo $bool ? 'Да' : 'Нет' (мы будем ее рассматривать на следующем уроке)

P. P. S. Пример разметки в следующем документе https://drive.google.com/open?id=1V3TOmLC95Bkh04eMKN9z6f0ozwduIgfd 
*/

$name = 'Andrzej Naidenovski';
$position = 'Программист PHP';
$phone = '+38 (097) 353-15-71';
$email = 'andtext007@gmail.com';
$salary ='10000 złoty';
$age = 53;
$experience = 1.5;
$city = 'Kraków';
$bool = true;
$description = <<<HEREDOC
Опытный программист PHP. Ищу работу в стабильной развивающейся компании. Обладаю необходимыми знаниями в
программировании на PHP, имею опыт работы с MVC фреймворками, такими как Laravel, Symfony, Yii2,
понимаю принципы ООП, SOLID, DRY, KISS, неплохо ориентируюсь в реляционных базах данных таких как MySQL и
PostgreSQL. Есть опыт работы в проектировании микросервисной архитектуры, написании RESTfull API приложений.
Владею знаниями Unix OS's на уровне администрирования систем.
HEREDOC;

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Основы языка PHP</title>
    <style>
        .wrapper {
            width: 740px;
            margin: 0 auto;
        }
        h1 {
            text-align: center;
        }
        .block {
            border-top: 1px solid #333333;
            padding: 20px 0;
            display: flex;
            justify-content: space-between;
        }

        .avatar {
            text-align: right;
            max-height: 60px;
        }
        img {
            max-height: 250px;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="header">
        <h2><?php echo $name; ?></h2>
        <h4><?php echo $position; ?></h4>
    </div>
    <div class="block">
        <div class="float-left">
            <p>Телефон для связи: <a href='tel: <?php echo $phone; ?>'><?php echo $phone; ?></a></p>
            <p>Эл. почта: <a href='mailto:<?php echo $email; ?>'><?php echo $email; ?></a></p>
            <p>Зарплатные ожидания: <?php echo $salary; ?></p>
            <p>Возраст: <?php echo $age; ?> года</p>
            <p>Опыт работы: <?php echo $experience; ?> года</p>
            <p>Город проживания: <?php echo $city; ?></p>
            <p>Готов к переезду: <?php echo $bool ? 'Да' : 'Нет'; ?></p>
        </div>
        <div class="avatar float-left">
            <img src="123.jpg">
        </div>
    </div>
    <div class="block">
<?php
echo $description;
?>
    </div>
</div>
</body>
</html>