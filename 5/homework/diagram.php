<?php

/*
 * Написать приложение для отображения столбчатой диаграммы,
 *      по оси X - год, начиная с 2000 до 2019, по оси Y какие-то рандомные значения от 1 до 300
 *
 * Предлагается написать 3 функции:
 * 1. Функция randArray должна возращать массив, ключами которого будет год с 2000 до текущего, значениями - радномные числа
 *      Для формирования массива можно использовать цикл for или любой другой удобный
 *      По желанию можно модифицировать условие так, чтобы  начало отсчета было не с 2000, а тоже рандомно,
*       например, с 2000 до 2010, то есть если рандомное число 2006, то строим диаграмму от 2006 до текущего года
 *
 * 2. Функция randColor должна возвращать строку рандомного цвета rgb, например "rgb(122, 12, 208)"
 *
 * 3. Функция makeDiagram должна принимать массив, который будет получен из функции randArray, и строить по нему столбчатую диаграмму.
 *      Столбики раскрасить в рандомный цвет, который можно получить путем вызова функции randColor
 *
 * Пример результата работы приложения можно посмотреть на рисунке diagram.png
 */

function randColor() {
    $r = rand(0, 255);
    $g = rand(0, 255);
    $b = rand(0, 255);
	$rgb = "rgb($r, $g, $b)";
	return  $rgb;
}

function randArray() {
	$year_start = rand(2000, 2005);
	$year_finish = date('Y') - 1;
	for ($i = $year_start; $i <= $year_finish; $i++) { 
		$height[$i] = rand(10, 300);
	}
	return $height;
}

function makeDiagram($array) {
	foreach ($array as $year => $height) {
		echo "<div class='block'>";
			echo "<div class='year'>" . $year . " г.</div>";
			echo "<div class='column' style='background-color: " . randColor() . "; height: " . $height . "px'></div>";
			echo "<div class='height'>$" . $height . "k</div>";
			echo "</div>";
		echo "</div>";
	}
}

?>

<html>
<head>
	<meta charset="utf-8">
    <title>Diagram</title>
    <style>
    	body {
    		margin: 30px;
		    display: flex;
		    justify-content: space-between;
    	}
    	.column {
    		width: 20px;
    		padding: 10px;
    		margin: 5px 0;
    	}
    	.diagram {
    		padding: 30px;
    	}
    	.block {
		    display: inline-flex;
		    padding: 10px;
		    /*border-bottom: 1px solid;*/
		    text-align: center;
		    flex-direction: column-reverse;
}
    </style>
</head>
<body>
    <?php echo makeDiagram(randArray()); ?>
</body>
</html>