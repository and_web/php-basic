<?php

/*
 * Написать рекурсивную функцию для нахождения факториала числа n. 
 * То есть функция должна принимать число n и возвращать произведение всех натуральных чисел от 1 до n.
 * Вызвать функцию с рандомным числом и вывести результат на экран в виде "Факториал числа n = П"
 * Для получения рандомного числа можно воспользоваться встроенной функцией rand(min, max), например rand(1, 100)
 */

$int = rand(1, 100);
function factorial($int) {
	if ($int == 1) {
		$result = 1;
	} else {
		$result = $int * factorial($int - 1);
	}
	return $result;
}
//$result = factorial($int);

?>

<html>
<head>
	<meta charset="utf-8">
    <title>Recursion</title>
</head>
<body>
    <div>
        <p>Факториал числа "<?php echo $int; ?>" равен <?php echo factorial($int); ?></p>
    </div>
</body>
</html>