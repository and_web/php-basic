<?php
/*
С помощью двух циклов for (один вложен в другой) написать алгоритм вывода на экран таблицы умножения
Оформить можно с помощью html элементов table или через кастомный css

Дополнительно. Выделить первый ряд и первую колонку дополнительным цветом

*/

?>

<!DOCTYPE html>

<html>
<head>
	<meta charset="utf-8">
    <title>Основы языка PHP</title>
    <style>

    </style>
</head>
<body>
<h1>Цикл for</h1>
<table border='1' cellspacing='0'>
    <?php 
    for($tr = 1; $tr <= 25; $tr++) {
        echo "<tr>";
            for($td = 1; $td <= 50; $td++) {
                if($tr == 1 or $td == 1) {
                    echo "<th style = 'background: #cecece'>" . ($tr * $td) . "</th>";
                } else {
                    echo "<td>" . $tr*$td . "</td>";
                }
            }
        echo "</tr>";    
    }
    ?>
</table>
</body>
</html>