<?php

/*
1. Создать переменную $users (массив), которая будет хранить пользователей, зарегистрированных в системе. Ключом должны быть логины, а значением - пароли
2. Написать форму авторизации с полями login и password
3. Когда приходит запрос методом POST, проверить есть ли у вас в массиве $users такой логин, если есть, нужно сравнить пароль с сохраненным у вас.
4. Если пароль совпадает, вывести на экран сообщение о том, что пользователь успешно залогинен
        В противном случае вывести на экран сообщение что что-то пошло не так
*/
$users = [
	'andrey' => '1111',
	'petr' => '2222', 
	'alex' => '3333',
	'sergey' => '4444',
];

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
</head>
<body>
	<form method="post">
		<label>Логин:</label><br />
		<input type='text' name='login' style="margin: 5px 0 20px 0;"><br />
		<label>Пароль:</label><br />
		<input type='password' name='password' style="margin: 5px 0 20px 0;"><br />
		<div class='buttons'>
			<button type='submit'>Войти</button>
			<button type='reset' class='close'>Отмена</button>
		</div>
	</form>
	<?php	

		if (!empty($_POST['login'])) {
			if (array_key_exists($_POST['login'], $users)) {
				if ($_POST['password'] == $users[$_POST['login']]) {
					echo "Вы успешно авторизованы";
                } else if (!empty($_POST['password'])) {
                    echo "Э, а ты точно не хакер?";
                } else {
                    echo "Пароль не может быть пуст";
                }
            } else {
                echo "Э, а ты точно не хакер? Пошел вон, если ты знаешь шо такое 'брут'";
            }
        } else {
			 echo "Введите логин и пароль";
		}
		
	?>

</body>
</html>