<?php
//if ($_GET['url'] == 'about') {
//    $color_about = "red";
//} else if ($_GET['url'] == 'contacts') {
//    $color_contacts = "red";
//} else {
//    $color_home = "red";
//}
ob_start();
if ($_GET['url'] == 'about') {
    include '/content/about.php';
} else if ($_GET['url'] == 'contacts') {
    include '/content/contacts.php';
} else {
    include '/content/home.php';
}
$content = ob_get_contents();
ob_end_clean();

?>

<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
</head>
<body>
    
<?php
    include '/menu.php';
    echo $content;
?>

</body>
</html>