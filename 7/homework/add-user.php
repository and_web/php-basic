<?php

/*
Написать модуль добавления нового пользователя в систему.
1. Написать форму, которая будет отправляться методом POST и будет включать следующие поля:
    a. Имя пользователя (текстовое поле)
    b. Фамилия пользователя (текстовое поле)
    c. Область проживания (выпадающий список)
    d. Город проживания (текстовое поле)
    e. Адрес проживания (текстовая область)
    f. Дата рождения (поле типа date)
2. Для хранения корректных значений областей, создать индексированный массив, содержащий все области Украины.
        В элементе select выводить элементы option из этого массива.
        Значениями option должны быть ключи массива, а в тексте должны быть значения элементов.
        Например <option value="21">Хмельницька область</option>
2. Написать код, который будет принимать и обрабатывать POST запрос.
3. Все входные параметры должны быть отфильтрованы с помощью встроенных функций, или пользовательскими функциями.
        В каждом параметре должны быть удалены все пробельные символы (в начале и конце), теги и преобразованы специальные символы.
4. Все входные параметры должны быть провалидированы с применением следующих правил:
    a) Имя, фамилия и город должны состоять минимум из 2 и максимум из 32 символов.
    b) Фамилия пользователя может быть двойная, например, Салтыков-Щедрин
    с) Города могут состоять из 2 слов через пробел, например Кривой Рог, или через дефис, например Ивано-Франковск
    d) Дата рождения должна быть корректной датой. Год не должен быть меньше 1900 и больше текущего года
    e) Область должна быть целым числом и значения данного параметра должно быть ключом в вашем массиве с областями.


5. Если какой-то из входящих параметров не проходит валидацию, нужно вывести соответствующее сообщение пользователю.
        В этом случае нужно снова вывести форму, с заполненными в прошлый раз данными (чтобы пользователь не вводил все данные заново).
        Также нужно выделить, например красными рамками элементы формы, которые не прошли валидацию.
6. Если все данные прошли валидацию нужно спрятать форму и вывести сообщение типа:
    "Пользователь {ИМЯ} {ФАМИЛИЯ}, {ДАТА}г. рождения, проживающий по адресу: {ОБЛАСТЬ}, г. {ГОРОД}, {АДРЕС}, был успешно добавлен в систему"
    Например, "Иванов Сергей, 22.10.1988г. рождения, проживающий по адресу: Житомирская область, г. Бердычев, ул. Пушкина, д. 21, был успешно добавлен в систему"
*/
ini_set('display_errors', 1);
error_reporting(E_ALL);

/*назначение переменных*/

$regions = [
    '1' => 'Киевская',
    '2' => 'Ивано-Франковская',
    '3' => 'Львовская',
    '4' => 'Одесская',
    '5' => 'Хмельницкая',
    '6' => 'Тернопольская',
    '7' => 'Винницкая',
    '8' => 'Закарпатская'
];

$firstname = isset($_POST['firstname']) ? filter($_POST['firstname']) : null;
$lastname = isset($_POST["lastname"]) ? filter($_POST["lastname"]) : null;
$city = isset($_POST["city"]) ? filter($_POST["city"]) : null;
$address = isset($_POST["address"]) ? filter($_POST["address"]) : null;
$key_region = isset($_POST["region"]) ? filter($_POST["region"]) : null;
$date = isset($_POST["date"]) ? filter($_POST["date"]) : null;

$message = '';
$display = "block";
$dateBirth = date("d.m.Y");

// определение функций
function filter($value) {
  $result = htmlspecialchars(strip_tags(stripslashes(trim($value))));
  return $result;
}

function validName($name) {
    if (iconv_strlen($name) < 2 OR iconv_strlen($name) > 32) {
        return false;
    }
    return true;
}
function validAddress($address) {
    if (iconv_strlen($address) < 7 OR iconv_strlen($address) > 64) {
        return false;
    }
    return true;
}
function validRegion($key_region, $regions) {
    if ($key_region > 0 AND $key_region < count($regions)) {
        return true;
    }
    return false;
}
function validBirthday($date) {
    if ($date) {
        $yearBirth = date("Y", strtotime($date));
        $yearNow = date("Y", time());
        if ($yearBirth >= 1900 && $yearBirth <= $yearNow) {
            return true;
        }
    }
    return false;
}
function borderColor($value) {
  $borderColor = (!$value AND !empty($_POST)) ? 'red' : '#e3e3e3';
  return $borderColor;
}
function message($value) {
  $message = (!$value AND !empty($_POST)) ? 'Заполните это поле' : '';
  return $message;
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <form method="POST" action="" class="form">
        <label name= "Имя">Имя:</label>
        <div class="flex">
            <input type="text" name="firstname" style="border: 1px solid <?php echo borderColor(validName($firstname)) ?>"
                   value="<?php echo $firstname; ?>">
            <div class="message"><?php echo message(validName($firstname)) ?></div>
        </div>
        <label name= "Фамилия">Фамилия:</label>
        <div class="flex">
            <input type="text" name="lastname" style="border: 1px solid <?php echo borderColor(validName($lastname)) ?>" value="<?php echo $lastname; ?>">
            <div class="message"><?php echo message(validName($lastname)) ?></div>
        </div>
        <label name= "Область">Область:</label>
        <div class="flex">
            <select size="1" name="region"
                    style="border: 1px solid <?php echo borderColor(validRegion($key_region, $regions)) ?>">
                <option selected disabled>Выберите область</option>
                
                <?php
                //if ($key_region){
                    //echo '<option selected value="' . $key_region . '">' . $regions[$key_region] . '</option>';
                //}
                foreach ($regions as $key => $value) {
                    echo '<option ' . ($key_region == $key ? 'selected' : '') . ' value="' . $key . '">' . $value . '</option>';
                }
                
                ?>
            </select>
            <div class="message"><?php echo message(validRegion($key_region, $regions)) ?></div>
        </div>
        <label name= "Город">Город:</label>
        <div class="flex">
            <input type="text" name="city" style="border: 1px solid <?php echo borderColor(validName($city)) ?>" value="<?php echo $city; ?>">
            <div class="message"><?php echo message(validName($city)) ?></div>
        </div>
        <label name= "Адрес">Адрес:</label>
        <div class="flex">
            <textarea name="address" style="border: 1px solid <?php echo borderColor(validAddress($address)) ?>"><?php echo $address ?></textarea>
            <div class="message"><?php echo message(validAddress($address)) ?></div>
        </div>
        <label name= "Дата рождения">Дата рождения:</label>
        <div class="flex">
            <input type="date" name="date" style="border: 1px solid <?php echo borderColor($date) ?>" value="<?php echo $date ?>">
            <div class="message"><?php echo message($date) ?></div>
        </div>    
        <div class='buttons'>
            <button type='submit'>Войти</button>
            <button class='close'>Отмена</button>
        </div>
    </form>

    <?php

    if (!empty($_POST)) {
        if (validName($firstname) AND validName($lastname) AND isset($regions[$key_region])
            AND validName($city) AND (validAddress($address)) AND validBirthday($date)) {
            $display = "none";
            echo "Пользователь $firstname $lastname, $dateBirth года рождения, проживающий по адресу: $regions[$key_region]
    	    область, г. $city, $address, был успешно добавлен в систему";
        } else {
            echo "Что-то пошло не так";
        }
    }
    
    ?>
	<style type="text/css">
		.form {
			display: <?php echo $display; ?>
		}
	</style>

</body>
</html>
