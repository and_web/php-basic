<?php
/*
Объявить переменную $rand и присвоить ей рандомное значение от 0 до 100 (вызов функции rand(0, 100))
Написать цикл while который выводит все числа от 0 до $rand. Каждое число должно быть в отдельной строке (перенос в html)
*/
$rand = rand(0, 100);

?>

<!DOCTYPE html>

<html>
<head>
	<meta charset="utf-8">
    <title>Основы языка PHP</title>
</head>
<body>
    <h1>Циклы while</h1>
    <p>
        Рандомное число: <?php /** @var TYPE_NAME $rand */
        echo $rand; ?>
        <br>
        <?php
            $i = 0;
			while ($i <= $rand) {
				echo "<p>$i</p>";
				$i++;
			}
        ?>
    </p>
</body>
</html>
