<?php

function readDirRecursive($dir) {
    $handle = opendir($dir);
    $files = [];
    while ($file = readdir($handle)) {
        if ($file == '.' || $file == '..') {
            continue;
        }
        $path = realpath($dir . '/' . $file);
        if (is_dir($path)) {
            $files = array_merge($files, readDirRecursive($path));
        } else {
            $files[] = $path;
        }
    }

    return $files;
}

$files = readDirRecursive('../../8');

echo '<pre>';
print_r($files);
echo '</pre>';