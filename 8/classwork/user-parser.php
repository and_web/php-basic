<?php

function parseUsers($csvFile) {
    $users = [];
    $string = file_get_contents($csvFile);
    $lines = explode("\n", $string);
    foreach ($lines as $key=>$line) {
        if (empty($line)) {
            continue;
        }
        if ($key == 0) {
            $titles = explode(',', $line);
            continue;
        }
        $user = explode(',', $line);
        $user = array_combine($titles, $user);
        $users[] = $user;
    }

    return $users;
}

$users = parseUsers('db.csv');

echo '<pre>';
print_r($users);
echo '</pre>';