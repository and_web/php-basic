<?php

ini_set('display_errors', 1);


function saveJson($file, $array) {
    $json = json_encode($array, JSON_UNESCAPED_UNICODE);

    $f = fopen($file, 'w');

    fwrite($f, $json);

    fclose($f);
}

function readJson($file) {
    $f = fopen($file, 'r');

    $json = fgets($f);

    $array = json_decode($json);

    return $array;
}

$users = [
    'last_name' => 'Petrovsky',
    'first_name' => 'Petr',
    'age' => 23,
];

saveJson('users_test.txt', $users);

$users = readJson('users_test.txt');

echo '<pre>';
print_r($users);
echo '</pre>';

