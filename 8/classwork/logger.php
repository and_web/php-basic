<?php

function logger($string, $level='info') {
    $handle = fopen('log', 'a');

    $time = date('d.m.Y H:i:s');
    $log = $time . ' [' . strtoupper($level) . '] ' . $string . "\n";
    fwrite($handle, $log);

    fclose($handle);
}

logger('We get request from user ' . $_SERVER['HTTP_USER_AGENT']);
logger('Access denied', 'error');