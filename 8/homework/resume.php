<?php

/*
Написать скрипт, для добавления, редактирования и вывода своего резюме (из урока №2).
Добавить возможность загружать свое фото через форму. Добавить возможность сохранять данные с формы в файл.
Добавить возможность читать файл и выводить данные на странице

1. Создать страницу для работы с резюме.
2. На страницу разместить меню со ссылками, определяющими режим работы. Должно быть 2 режима - view и edit. По умолчанию view
        Если запрос на страницу приходит с режимом edit, необходимо показывать форму редактирования резюме.
        Если запрос на страницу приходит с режимом view, необходимо выводить сохраненное резюме.
3. Создать форму со всеми элементами, необходимыми для резюме
        Добавить в форму input с типом file, для загрузки своего фото
4. При загрузке формы проверить все входные параметры. Создать ассоциативный массив и присвоить ему значения из входных параметров.
        То есть должен быть массив вида:
        $resume = [
            'first_name' => 'Данные из формы',
            'age' => 'Данные из формы',
        ]
5. Если пришел файл, проверить его на безопасность, и сохранить в отдельной директории, созданной для хранения картинок
        Если сохранение файла прошло успешно, в массив $resume добавить элемент с ключом 'photo', значением которого должен быть URI загруженного файла.
        Например, $resume['photo'] = 'images/my_file_name.png'
6. Сохранить массив в файл в сериализованном виде или в JSON формате.
7. В режиме view почитать данные из файла, преобразовать в массив и вывести на экран

*/

?>

<!DOCTYPE html>
<html>
<head>
	<title>Resume</title>
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>
    <?php
    $active_edit = "";
    $active_view = "";
    if ($_GET['url'] == 'edit') {
        $active_edit = "active";
    } else {
        $active_view = "active";
    }
    ?>
    <div class="mode">
        <a href="http://php-basic/8/homework/resume.php?url=view" class="btn btn-outline-success <?=$active_view ?>"
           role="button">View</a>
        <a href="http://php-basic/8/homework/resume.php?url=edit" class="btn btn-outline-primary <?=$active_edit ?>"
           role="button">Edit</a>
    </div>

    <?php
    if ($_GET['url'] == 'edit') {
        include '/inc/edit.php';
    } else {
        include '/inc/view.php';
    }
    ?>

</body>
</html>