<?php

function filter($value) {
    return htmlspecialchars(strip_tags(stripslashes(trim($value))));
}
function saveJson($file, $array) {
    $json = json_encode($array, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    $f = fopen($file, 'w');
    fwrite($f, $json);
    fclose($f);
}
function readJson($file) {
    $f = fopen($file, 'r');
    $json = fread($f, 1000000);
    $array = json_decode($json, true);
    fclose($f);
    return $array;
}
//00000110
// |
//00000101
// =
//00000111

//00000010
// ~
// =
//11111101

//00000110
// ^
//00000101
// =
//00000011
