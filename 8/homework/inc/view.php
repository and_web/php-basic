<h1>Резюме (просмотр)</h1>

<?php
require_once 'functions.php';
$resume = readJson('inc/resume.txt');
?>

<style>
    .wrapper {
        width: 740px;
        margin: 0 auto;
    }
    h1 {
        text-align: center;
    }
    .block {
        border-top: 1px solid #333333;
        padding: 20px 0;
        display: flex;
        justify-content: space-between;
    }

    .avatar {
        text-align: right;
        max-height: 60px;
    }
    img {
        max-height: 250px;
    }
</style>

<div class="wrapper">
    <div class="header">
        <h2><?php echo $resume['name']; ?></h2>
        <h4><?php echo $resume['position']; ?></h4>
    </div>
    <div class="block">
        <div class="float-left">
            <p>Телефон для связи: <a href='tel: <?php echo $resume['phone']; ?>'><?php echo $resume['phone']; ?></a></p>
            <p>Эл. почта: <a href='mailto:<?php echo $resume['email']; ?>'><?php echo $resume['email']; ?></a></p>
            <p>Зарплатные ожидания: <?php echo $resume['salary']; ?></p>
            <p>Возраст: <?php echo $resume['age']; ?> года</p>
            <p>Опыт работы: <?php echo $resume['experience']; ?> года</p>
            <p>Город проживания: <?php echo $resume['city']; ?></p>
            <p>Готов к переезду: <?php echo $resume['relocation'] ? 'Да' : 'Нет'; ?></p>
        </div>
        <div class="avatar float-left">
            <img src="<?php echo $resume['photo']; ?>">
        </div>
    </div>
    <div class="block">
<?php
echo <<<HEREDOC
Опытный программист PHP. Ищу работу в стабильной развивающейся компании. Обладаю необходимыми знаниями в
программировании на PHP, имею опыт работы с MVC фреймворками, такими как Laravel, Symfony, Yii2,
понимаю принципы ООП, SOLID, DRY, KISS, неплохо ориентируюсь в реляционных базах данных таких как MySQL и
PostgreSQL. Есть опыт работы в проектировании микросервисной архитектуры, написании RESTfull API приложений.
Владею знаниями Unix OS's на уровне администрирования систем.
HEREDOC;
        ?>
    </div>
</div>
