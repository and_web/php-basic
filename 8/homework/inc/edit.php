<?php
require_once 'functions.php';
$resume = readJson('inc/resume.txt');
?>

<h1>Резюме (редактор)</h1>
<p>Претендент, неспособный заполнить поля формы резюме, будет автоматически удален из конкурса и забанен на срок 1,5 года!</p>

<form method="post" action="inc/handler.php" enctype="multipart/form-data" class="container">
    <div class="form-group row">
        <label for="name" class="col-sm-3 col-form-label">ФИО</label>
        <div class="col-sm-9">
            <input name="name" type="text" class="form-control" id="name" value="<?php echo $resume['name']; ?>">
        </div>
    </div>
    <div class="form-group row">
        <label for="position" class="col-sm-3 col-form-label">Название желаемой должности</label>
        <div class="col-sm-9">
            <input name="position" type="text" class="form-control" id="position" value="<?php echo $resume['position']; ?>">
        </div>
    </div>
    <div class="form-group row">
        <label for="phone" class="col-sm-3 col-form-label">Телефон для связи</label>
        <div class="col-sm-9">
            <input name="phone" type="text" class="form-control" id="phone" value="<?php echo $resume['phone']; ?>">
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-3 col-form-label">Email</label>
        <div class="col-sm-9">
            <input name="email" type="email" class="form-control" id="inputEmail3" value="<?php echo $resume['email']; ?>">
        </div>
    </div>
    <div class="form-group row">
        <label for="age" class="col-sm-3 col-form-label">Возраст</label>
        <div class="col-sm-9">
            <input name="age" type="text" class="form-control" id="age" value="<?php echo $resume['age']; ?>">
        </div>
    </div>
    <div class="form-group row">
        <label for="experience" class="col-sm-3 col-form-label">Опыт работы</label>
        <div class="col-sm-9">
            <input name="experience" type="text" class="form-control" id="experience" value="<?php echo $resume['experience']; ?>">
        </div>
    </div>

    <div class="form-group row">
        <label for="exampleFormControlTextarea1" class="col-sm-3">Информация о себе</label>
        <textarea name="about" class="form-control col-sm-9" id="about" rows="5"><?php echo $resume['about']; ?></textarea>
    </div>

    <div class="form-group row">
        <label for="city" class="col-sm-3 col-form-label">Город проживания</label>
        <div class="col-sm-9">
            <input name="city" type="text" class="form-control" id="city" value="<?php echo $resume['city']; ?>">
        </div>
    </div>

    <?php $tt = $resume['salary'] == 'less $1k' ? 'checked' : ''; ?>
    <?php $dd = $resume['salary'] == 'more $1k' ? 'checked' : ''; ?>
    <?php $ss = $resume['relocation'] == 'on' ? 'checked' : ''; ?>
    <fieldset class="form-group">
        <div class="row">
            <legend class="col-form-label col-sm-3 pt-0">Зарплатные ожидания</legend>
            <div class="col-sm-9">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="salary" id="gridRadios1" value="less $1k"
                           <?php echo $tt; ?>>
                    <label class="form-check-label" for="gridRadios1">
                        less $1k
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="salary" id="gridRadios2" value="more $1k"
                        <?php echo $dd ?>>
                    <label class="form-check-label" for="gridRadios2">
                        more $1k
                    </label>
                </div>
                <div class="form-check disabled">
                    <input class="form-check-input" type="radio" name="salary" id="gridRadios3" value="option3" disabled>
                    <label class="form-check-label" for="gridRadios3">
                        Готов работать бесплатно
                    </label>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="form-group row">
        <div class="col-sm-3">Готовность к переезду</div>
        <div class="col-sm-9">
            <div class="form-check">
                <input name="relocation" class="form-check-input" type="checkbox" id="relocation" <?php echo $ss ?>>
                <label class="form-check-label" for="gridCheck1">
                    Да
                </label>
            </div>
        </div>
    </div>

    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupFileAddon01">Загрузить</span>
        </div>
        <div class="custom-file">
            <input type="hidden" name="MAX_FILE_SIZE" value="5000000" />
            <input name="file_1" type="file" class="custom-file-input" id="inputGroupFile01"
                   aria-describedby="inputGroupFileAddon01">
            <label class="custom-file-label" for="inputGroupFile01">Прикрепите свое фото</label>
        </div>
    </div>

    <div class="form-group">
        <label for="exampleFormControlFile1">Ваше фото</label>
        <input type="hidden" name="MAX_FILE_SIZE" value="5000000" />
        <input name="file_2" type="file" class="form-control-file" id="exampleFormControlFile1">
    </div>

    <div class="form-group row">
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
    </div>
</form>