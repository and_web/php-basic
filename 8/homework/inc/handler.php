<?php
require_once 'functions.php';
//require_once 'data.php';

$name = isset($_POST["name"]) ? filter($_POST["name"]) : null;
$position = isset($_POST["position"]) ? filter($_POST["position"]) : null;
$phone = isset($_POST["phone"]) ? filter($_POST["phone"]) : null;
$email = isset($_POST["email"]) ? filter($_POST["email"]) : null;
$age = isset($_POST["age"]) ? filter($_POST["age"]) : null;
$experience = isset($_POST["experience"]) ? filter($_POST["experience"]) : null;
$about = isset($_POST["about"]) ? filter($_POST["about"]) : null;
$city = isset($_POST["city"]) ? filter($_POST["city"]) : null;
$salary = isset($_POST["salary"]) ? filter($_POST["salary"]) : null;
$relocation = isset($_POST["relocation"]) ? filter($_POST["relocation"]) : null;

$resume = [
    'name' => $name,
    'position' => $position,
    'phone' => $phone,
    'email' => $email,
    'age' => $age,
    'experience' => $experience,
    'about' => $about,
    'city' => $city,
    'salary' => $salary,
    'relocation' => $relocation,
];

if($_FILES["file_2"]["name"]) {
    $upload_dir = '../assets/images';
    $file_name = basename($_FILES["file_2"]["name"]);
    $file_path = "$upload_dir/$file_name";

    if (move_uploaded_file($_FILES['file_2']['tmp_name'], $file_path )){
        $resume['photo'] = "/8/homework/assets/images/$file_name";
    }
} else {
    $resume['photo'] = readJson('resume.txt')['photo'];
}

saveJson('resume.txt', $resume);
header('Location: http://php-basic/8/homework/resume.php?url=view');

?>