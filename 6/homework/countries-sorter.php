<?php

/*
Написать функцию, которая принимает первым параметром строку, содержащую список стран, разделенных запятыми.
Функция должна отсортировать страны в алфавитном порядке.
Вторым параметром функция принимает разделитель и возвращает строку, содержащую отсортированные страны с указанным разделителем
Второй параметр необязательный, и по умолчанию страны будут разделяться определенным на ваше усмотрение символом
*/
//проблема с USA:
$string = 'Mayotte, Polska, England, USA, Zimbabwe, United State of America, Africa, Uganda, Mexico, Argentina, Ukraine';
//$string = 'Mayotte, Polska, England, Usa, Zimbabwe, United State of America, Africa, Uganda, Mexico, Argentina, Ukraine';

function abc_string($string, $delim = ' ||| ') {
	$array = explode(', ', $string);
	sort($array, SORT_NATURAL | SORT_FLAG_CASE );
	$array_string = implode($delim, $array);
	echo $array_string;
}
abc_string($string, ' : ');