<?php

/*
Написать функцию-конвертер строки.
На входе принимается строка, и режим преобразования.
В зависимости от режима, фукнция будет преобразовывать и возвращать строку:
    1) все символы в верхнем регистре (upper)
    2) все символы в нижнем регистре (lower)
    3) первый символ строки в верхнем регистре, все остальные в нижнем регистре (one_upper)
    4) первые символы каждого слова в верхнем регистре, все остальные в нижнем регистре (one_upper_every_words)
    5) инвертированый регистр всех символов (invert)
*/

// mb_convert_case

function converter($string, $mode) {
	if ($mode == 'upper') {
	    $string = mb_strtoupper($string);
	} elseif ($mode == 'lower') {
		$string = mb_strtolower($string);
	} elseif ($mode == 'one_upper') {
		$string = ucfirst(mb_strtolower($string));
	} elseif ($mode == 'one_upper_every_words') {
		$string = ucwords(mb_strtolower($string));
	} elseif ($mode == 'invert') {
		$string = invertCase($string);
	}
	return $string;
}

function invertCase($str) {
	$str_len = strlen($str);
    $invert_str = '';
	for($i = 0; $i < $str_len; $i++){
	       if(!(is_numeric($str[$i]))){            
	           if($str[$i]===(strtoupper($str[$i]))){
	               $invert_str .= strtolower($str[$i]); 
	           } else{                              
	               $invert_str .= strtoupper($str[$i]);
	           }
	       } else{             
	           $invert_str .= $str[$i];
	       }
	}
	return $invert_str;
}

echo converter('aLex sfKSSJKskfl Lklkasflkl', 'invert');

echo "<br />";
function max_int($int_1, $int_2 = 20) {
 if ($int_1 > $int_2) {
 	echo($int_1);
 } elseif ($int_1 < $int_2) {
 	echo($int_2);
 } else {
 	echo("Эти числа равны");
 }
}

$arg = 20;
$age = 20;
max_int(30);

function maximum($a, $b){
	// Как сделать при $a == $b; с помощью тернарного оператора???
	return ($a < $b) ? $b : ($a > $b ? $a : "равны");
	$c = $a - $b;
	if ($c > 0) {
		return $a;
	} elseif ($c == 0) {
		echo "числа равны";
	} else {
		return $b;
	}
}
echo "<br />";
echo (maximum(70, 70));


?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
</head>
<body>

</body>
</html>