<?php

/*
Написать функцию, которая принимает массив. Значения элементов массива могут повторяться.
Например, $array = [1, 34, 22, 12, 6, 22, 45, 14, 34, 34, 10]
Функция должна возвращать количество уникальных элементов
*/

$array = [1, 34, 22, 12, 6, 22, 45, 14, 34, 34, 10];

function countUniqElements($array){
	$count = count(array_unique($array));
	return $count;
}
echo countUniqElements($array);