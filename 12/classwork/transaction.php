<?php

ini_set('display_errors', 1);
$user = [
    'first_name' => 'Андрей',
    'last_name' => 'Андреев',
    'middle_name' => 'Андреевич',
    'number' => 'YU123456',
    'issue_date' => date('d.m.Y H:i:s'),
    'issue_by' => 'Чернигов',
];

$link = mysqli_connect('localhost', 'root', '', 'phpbasic');
mysqli_set_charset($link, 'utf8');

//mysqli_query($link, "START TRANSACTION");
mysqli_begin_transaction($link);

$query = "INSERT INTO users (first_name, last_name, middle_name) VALUES("
    . "'" . $user['first_name'] . "', '" . $user['last_name'] . "', '" . $user['middle_name'] . "')";

//echo $query . "<br>";
$resultUser = mysqli_query($link, $query);

if ($resultUser) {
    echo 'User insert ok';
    $resultPassport = mysqli_query($link, "INSERT INTO passport (number, issue_date, issue_by) VALUES("
        . "'" . $user['number'] . "', '" . $user['issue_date'] . "', '" . $user['issue_by'] . "')");
    if ($resultPassport) {
        echo 'Passport insert ok';
        mysqli_commit($link);
        //mysqli_query($link, "COMMIT");
    } else {
        echo 'Passport insert error';
        mysqli_rollback($link);
        //mysqli_query($link, "ROLLBACK");
    }
} else {
    echo 'User insert error';
}
