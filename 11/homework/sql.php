<?php

//1.	Написать запросы на создание двух таблиц для будущего интернет-магазина. Нужно создать таблицы categories и products.
//2.	Сделать поле id первичным ключом и с автоинкрементом.
//3.	Таблица products должна содержать поле для связи с таблицей categories.

//$link = mysqli_connect('127.0.0.1', 'root', '');
//if (mysqli_query($link, 'CREATE DATABASE phpbasic') === true) {
//    echo "База phpbasic успешно создана <br />";
//} else {
//    echo 'Ошибка при создании базы данных: ' . mysqli_error($link) . "<br />";
//}

$link = mysqli_connect('127.0.0.1', 'root', '', 'phpbasic');
if (!$link) {
    die('Ошибка соединения: ' . mysqli_error());
}
mysqli_set_charset($link, 'utf8');

//if (mysqli_query($link, 'CREATE TABLE categories (
//          id INT(11) AUTO_INCREMENT,
//          name VARCHAR (255),
//          PRIMARY KEY (id)
//        )') === true) {
//    echo "Таблица categories успешно создана <br />";
//} else {
//    echo 'Ошибка при создании таблицы categories: ' . mysqli_error($link) . "<br />";
//}
//
//if (mysqli_query($link, 'CREATE TABLE products (
//          id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
//          name VARCHAR (255),
//          category_id INT(11),
//          FOREIGN KEY (category_id) REFERENCES categories (id)
//        )') === true) {
//    echo "Таблица products успешно создана <br />";
//} else {
//    echo 'Ошибка при создании таблицы products: ' . mysqli_error($link) . "<br />";
//}

//4.	Написать запросы для вставки нескольких записей в обе таблицы. (Добавить записи в обе таблицы)

function insertValuesToCategories ($id, $name) {
    global $link;
    if (mysqli_query($link, "INSERT INTO categories (id, name) VALUES ($id, '$name');") === true){
        echo "Строка в таблицу categories успешно добавлена<br />";
    } else {
        echo "Ошибка при добавлении данных в таблицу categories: " . mysqli_error($link) . "\n";
    }
}
function insertValuesToProducts ($id, $name, $category_id) {
    global $link;
    if (mysqli_query($link, "INSERT INTO products (id, name, category_id) VALUES ($id, '$name', $category_id);") === true){
        echo "Строка в таблицу Products успешно добавлена<br />";
    } else {
        echo "Ошибка при добавлении данных в таблицу products: " . mysqli_error($link) . "\n";
    }
}
/*
$values = [
    'id' => 100,
    'name' => 'iphone',
    'cat_id' => 5,
];
*/
function insert($table, $values) {
    global $link;
    $sql = "INSERT INTO $table (" . array_keys($values) . ") VALUES (" . array_values($values) . ")";
//    if (mysqli_query($link, $sql)) {
//        return true;
//    }
//    return false;
    return (bool) mysqli_query($link, $sql);
}
//insertValuesToCategories(1, 'Компьютеры');
//insertValuesToCategories(2, 'Ноутбуки');
//insertValuesToCategories(3, 'Телевизоры');
//insertValuesToCategories(4, 'Планшеты');
//insertValuesToCategories(5, 'Смартфоны');
//insertValuesToProducts(1, 'any computer', 1);
//insertValuesToProducts(2, 'any notebook', 2);
//insertValuesToProducts(3, 'asus', 2);
//insertValuesToProducts(4, 'xiaomi', 5);
//insertValuesToProducts(5, 'nokia', 5);
//insertValuesToProducts(6, 'Meizu', 5);
//insertValuesToProducts(7, 'PIPO', 4);

$categories_list = [
    1 => 'Компьютеры',
    2 => 'Ноутбуки',
    3 => 'Телевизоры',
    4 => 'Планшеты',
    5 => 'Смартфоны'
];

$products = [
    1 => [
        '1'=> 'any computer'
    ],
    2 => [
        '2'=> 'any notebook'
    ],
    3 => [
        '2'=> 'asus'
    ],
    4 => [
        '5'=> 'xiaomi'
    ],
    5 => [
        '5'=> 'nokia'
    ],
    6 => [
        '5'=> 'Meizu'
    ],
    7 => [
        '4'=> 'PIPO'
    ],
];

foreach ($categories_list as $key => $cat) {
    insertValuesToCategories($key, $cat);
}
foreach ($products as $key => $product_list) {
    foreach ($product_list as $cat => $prod) {
        insertValuesToProducts($key, $prod, $cat);
    }
}

//5.	Написать запрос на выборку данных из таблицы categories (получить список категорий) с ограничением количества результатов.

//$result = mysqli_query($link, 'SELECT name FROM categories ORDER BY id DESC LIMIT 3');
//$array = array_column(mysqli_fetch_all($result, MYSQLI_ASSOC), 'name');
//echo "<br />";
//print_r($array);
//echo "<br />";

//6.	Написать запрос на выборку данных из таблицы products (получить список продуктов) с фильтрацией по id категории и ограничением количества результатов.

//$result = mysqli_query($link, 'SELECT name FROM products WHERE category_id=5 ORDER BY id ASC LIMIT 2');
//$array = array_column(mysqli_fetch_all($result, MYSQLI_ASSOC), 'name');
//echo "<br />";
//print_r($array);
//echo "<br />";


//7.	Написать запрос на выборку products с присоединением таблицы categories. В результате должны быть поля из products и название категории, к которой принадлежит каждый товар

//$result = mysqli_query($link, 'SELECT * FROM products LEFT JOIN categories ON categories.id = products.category_id');
// правильный запрос:
//$result = mysqli_query($link, 'SELECT products.*, categories.name AS category_name FROM products LEFT JOIN categories ON categories.id = products.category_id');

////!!! ТУТ непонятно (исправлено выше):
//$array = array_column(mysqli_fetch_all($result, MYSQLI_ASSOC), 'name');
//echo "<br />";
//print_r($array);
//echo "<br />";

//8.	Написать запрос на выборку всех категорий с присоединением поля с подсчётом количества товаров в каждой категории.

$result = mysqli_query($link, 'SELECT `categories`.`name`, COUNT(`products`.`category_id`) AS items
                                    FROM `products`
                                    LEFT JOIN `categories`
                                    ON `products`.`category_id` = `categories`.`id`
                                    GROUP BY `categories`.`id`');
//$array = mysqli_fetch_all($result, MYSQLI_ASSOC);
//echo "<br />";
//print_r($array);
//echo "<br />";

?>