<?php

/*
Имеется массив, содержащий список урлов различных сайтов.
Необходимо написать функцию, которая будет определять урлы, которые находятся в одной доменной зоне.
То есть, нужно получить все урлы с одинаковым доменом верхнего уровня, например все урлы в доменной зоне ua.

Функция на входе должна принимать два параметра:
1) массив урлов
2) строку с доменном верхнего уровня, например 'ua'.

На выходе должна отдавать массив, содержащий только элементы, которые находятся в указанной доменной зоне
*/


function getUrl($array, $domain) {
    $result = [];
    foreach ($array as $string) {
        $pattern ='/\.' . $domain . '$/u';
        if (preg_match($pattern, $string, $matches)) {
            $result[] = $string;
        }
    }
    return $result;
}

$array = [
    'https://itea.ua',
    'https://wikipedia.org',
    'https://taobao.com',
    'https://rozetka.com.ua',
    'https://www.google.com',
    'https://www.amazon.com',
    'https://www.php.net',
    'https://allegro.pl',
    'https://telegram.org',
    'https://www.ebay.com',
    'https://www.mobile.de',
    'https://prom.ua',
    'https://aliexpress.com',
    'https://www.ukr.net',
    'https://www.ukrua',
];

$domain = 'ua';

echo "Найдены урлы с доменном $domain:";
foreach(getUrl($array, $domain) as $value){
    echo '<pre>';
    print_r($value);
    echo '</pre>';
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>


</body>
</html>