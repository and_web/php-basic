<?php

/*
Сделать темную и светлую тему для своего сайта используя cookie
1. Добавить на страницах выбор темы (ссылки с GET параметрами).
2. При выборе темы (клике на ссылку) установить куку в браузере
3. Отображать страницу при последующих запросах с соответствующей темой
*/
if (isset($_GET['theme'])) {
	$theme = $_GET['theme'];
	setcookie('theme', $_GET['theme']);
} else {
	$theme = isset($_COOKIE['theme']) ? $_COOKIE['theme'] : 'light';
}
 
?>

<!DOCTYPE html>

<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <style>
        body.dark {
            background-color: #202020;
            color: #ffffff;
        }
        body.dark a {
            color: yellow;
        }
        body.light {
            background-color: #ffffff;
            color: #202020;
        }
    </style>
</head>
<body class="<?php echo $theme ?>">
    <h1>Основы языка PHP</h1>
    <p>Выбери <a href="/9/labs/cookie-theme.php/?theme=light">светлую</a> или
        <a href="/9/labs/cookie-theme.php/?theme=dark">темную</a> тему</p>

    <p>
        PHP, расшифровывающийся как "PHP: Hypertext Preprocessor" - «PHP: Препроцессор Гипертекста»,
        является распространенным интерпретируемым языком общего назначения с открытым исходным кодом.
        PHP создавался специально для ведения web-разработок и код на нем может внедряться непосредственно в HTML-код.
        Синтаксис языка берет начало из C, Java и Perl, и является легким для изучения.
        Основной целью PHP является предоставление web-разработчикам возможности быстрого создания динамически генерируемых
        web-страниц, однако область применения PHP не ограничивается только этим.
    </p>
</body>
</html>